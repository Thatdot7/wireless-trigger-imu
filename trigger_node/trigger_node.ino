#include <SPI.h>

#include <nRF24L01.h>
#include <printf.h>
#include <RF24.h>
#include <RF24_config.h>



// Transmission Channel (1-125)
#define NRF24L01_CH 76

// Commands
#define TOGGLE_OUTPUT_HIGH_CMD 0x34
#define TOOGLE_OUTPUT_LOW_CMD 0x56
#define TEST_CMD 0x75

// Broadcast Address (5 bytes)
#define NRF24L01_ADDR {0xE8, 0xE8, 0xF0, 0xF0, 0xE2}

RF24 radio(7,8);
unsigned long start_time, current_time;
uint8_t addrtx0[5] = NRF24L01_ADDR;

void setup() {
  // put your setup code here, to run once:
  Serial.begin(115200);
   while (!Serial) {
    ; // wait for serial port to connect. Needed for Leonardo only
  }

  radio.begin();
  radio.openWritingPipe(addrtx0);
  radio.printDetails();
 
}

void loop() {
  // put your main code here, to run repeatedly:
   if(Serial.available() > 0) {
    char incoming = Serial.read();
    uint8_t message;
    if(incoming == '1') {
      message = TOGGLE_OUTPUT_HIGH_CMD;
      if (radio.write(&message, 1)){
       Serial.println("Toggle HIGH");
      }
    } else if(incoming == '0') {
      message = TOOGLE_OUTPUT_LOW_CMD;
      if (radio.write(&message, 1)){
       Serial.println("Toggle LOW");
      }
    }
  }
}
