#include <nRF24L01.h>
#include <printf.h>
#include <RF24.h>
#include <RF24_config.h>

#include <Wire.h>
#include <SPI.h>
#include "SdFat.h"
#include "FreeStack.h"

#include "SparkFun_BNO080_Arduino_Library.h"

#ifdef __AVR_ATmega328P__
#include "MinimumSerial.h"
MinimumSerial MinSerial;
#define Serial MinSerial
#endif  // __AVR_ATmega328P__

//==============================================================================
// Start of configuration constants.
//==============================================================================
// Abort run on an overrun.  Data before the overrun will be saved.
#define ABORT_ON_OVERRUN 1
//------------------------------------------------------------------------------
//Interval between data records in microseconds.
const uint32_t LOG_INTERVAL_USEC = 10000;
//------------------------------------------------------------------------------
// Set USE_SHARED_SPI non-zero for use of an SPI sensor.
// May not work for some cards.
#define USE_SHARED_SPI

#ifndef USE_SHARED_SPI
#define USE_SHARED_SPI 0
#endif  // USE_SHARED_SPI
//------------------------------------------------------------------------------
// Pin definitions.
//
const uint8_t PWR_LED = 1;
const uint8_t OUT_LED = 2;
const uint8_t TEST_LED = 3;

struct data_t {
  unsigned long time;
  float quatI;
  float quatJ;
  float quatK;
  float quatReal;
  float quatRadianAccuracy;
};

bool messagePrinted = false;

//Function Prototypes
void acquireData(data_t* data);
void printData(Print* pr, data_t* data);
void printHeader(Print* pr);
void userSetup();

void initSD();
void logData();
void binaryToCsv();

void initRadio();
void check_radio(void);

void setup() {
  Serial.begin(115200);

  //pinMode(write_flag, INPUT_PULLUP);
  pinMode(PWR_LED,OUTPUT);
  pinMode(OUT_LED,OUTPUT);
  pinMode(TEST_LED,OUTPUT);
  digitalWrite(PWR_LED, HIGH);
  digitalWrite(OUT_LED, LOW);
  digitalWrite(TEST_LED, LOW);

  
  // Setup sensors.
  userSetup();
  initSD();

  initRadio();

}

void loop() {
//  delay(3000);
  if (digitalRead(OUT_LED)==HIGH){
    
    Serial.println("Recording Data...");
    logData();
    digitalWrite(TEST_LED, HIGH);
    binaryToCsv();
    digitalWrite(TEST_LED, LOW);
    Serial.println(""); Serial.println("Saved .csv file");
    Serial.println("");
    messagePrinted = false;
  } else {
    if(!messagePrinted) {
      messagePrinted = true;
      Serial.print("Change Digital Pin ");
      Serial.print(OUT_LED);
      Serial.println(" to HIGH to Record Data...");
    }
  }
}
