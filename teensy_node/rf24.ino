

// Transmission Channel (1-125)
#define NRF24L01_CH 76

// Commands
#define TOGGLE_OUTPUT_HIGH_CMD 0x34
#define TOOGLE_OUTPUT_LOW_CMD 0x56
#define TEST_CMD 0x75

// Broadcast Address (5 bytes)
#define NRF24L01_ADDR {0xE8, 0xE8, 0xF0, 0xF0, 0xE2}

#define SOFTSPI
RF24 radio(5,6);

uint8_t addrtx0[5] = NRF24L01_ADDR;

void initRadio() {
  pinMode(5, OUTPUT);
  pinMode(6, OUTPUT);
  radio.begin();
  radio.openReadingPipe(1, addrtx0);
  
  attachInterrupt(0, check_radio, LOW);
  radio.startListening();
  radio.printDetails();
}

void check_radio(void)                                // Receiver role: Does nothing!  All the work is in IRQ
{
  bool tx,fail,rx;
  radio.whatHappened(tx,fail,rx);                     // What happened?

  if ( rx || radio.available()){                      // Did we receive a message?
    uint8_t message;
    radio.read(&message, 1);
    if(message == TOGGLE_OUTPUT_HIGH_CMD) {
      Serial.println("Toggle HIGH");
      digitalWrite(OUT_LED, HIGH);
    }
    else if(message == TOOGLE_OUTPUT_LOW_CMD) {
      digitalWrite(OUT_LED, LOW);
      Serial.println("Toggle LOW");
    }
  }
}
