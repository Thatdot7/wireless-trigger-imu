
BNO080 myIMU;
static uint32_t startMicros;

//
//struct data_t {
//  unsigned long time;
//  float quatI;
//  float quatJ;
//  float quatK;
//  float quatReal;
//  float quatRadianAccuracy;
//};
//


// Acquire a data record.
void acquireData(data_t* data) {
  if (myIMU.dataAvailable() == true)
  {
    data->time = micros();
    data->quatI = myIMU.getQuatI(); 
    data->quatJ = myIMU.getQuatJ();
    data->quatK = myIMU.getQuatK();
    data->quatReal = myIMU.getQuatReal();
    data->quatRadianAccuracy = myIMU.getQuatRadianAccuracy();
  }
}

// setup AVR I2C
void userSetup() {
  Wire.begin();
  Wire.setClock(400000);
  if (myIMU.begin() == false)
  {
    Serial.println("BNO080 not detected at default I2C address.");
    while (1);
  }

 myIMU.enableRotationVector(10); //Send data update every 10ms
}

// Print a data record.
void printData(Print* pr, data_t* data) {
  if (startMicros == 0) {
    startMicros = data->time;
  }
  pr->print(data->time- startMicros);
  pr->write(',');
  pr->print(data->quatI);
  pr->write(',');
  pr->print(data->quatJ);
  pr->write(',');
  pr->print(data->quatK);
  pr->write(',');
  pr->print(data->quatReal);
  pr->write(',');
  pr->println(data->quatRadianAccuracy);
}

// Print data header.
void printHeader(Print* pr) {
  startMicros = 0;
  pr->println(F("micros,quatI,quatJ,quatK,quatReal,quatRadianAccuracy"));
}
